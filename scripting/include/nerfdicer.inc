#if defined _nerfdicer_included
  #endinput
#endif
#define _nerfdicer_included

forward void NerfDicer_OnSpawnNerf(int client, char[] nerf);

native int NerfDicer_AddNerf(char nerf[32], int chance);

public void __pl_nerfdicer_SetNTVOptional() 
{
	MarkNativeAsOptional("NerfDicer_AddNerf");
}