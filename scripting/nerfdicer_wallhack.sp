#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Nerf Dicer - Wallhack"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Nerf Dicer - Wallhack"
#define PLUGIN_URL "zipcore.net"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

ConVar g_cvChance;
int g_iChance;

ConVar g_cvTime;
float g_fTime;

#include <sourcemod>
#include <sdktools>
#include <multicolors>
#include <nerfdicer>

public void OnPluginStart()
{
	CreateConVars();
	
	NerfDicer_AddNerf("wallhack", g_iChance);
	
	HookEvent("player_spawn", Event_Spawn);
}

/* ConVars */

void CreateConVars()
{
	g_cvChance = CreateConVar("nerfdicer_wallhack_chance", "1", "Chance for the wallhack nerf.");
	g_iChance = GetConVarInt(g_cvChance);
	HookConVarChange(g_cvChance, Action_OnSettingsChange);
	
	g_cvTime = CreateConVar("nerfdicer_wallhack_time", "999.0", "Amount of time to enable wallhack from player spawn (set it too a very high number if you like to have it the full round enabled).");
	g_fTime = GetConVarFloat(g_cvTime);
	HookConVarChange(g_cvTime, Action_OnSettingsChange);
	
	AutoExecConfig(true, "nerfdicer-wallhack");	
}

public void Action_OnSettingsChange(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	if (cvar == g_cvChance)
	{
		g_iChance = StringToInt(newvalue);
		NerfDicer_AddNerf("wallhack", g_iChance);
	}
}

/* Events */

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	SetEntPropFloat(client, Prop_Send, "m_flDetectedByEnemySensorTime", 0.0);
	
	return Plugin_Continue;
}

public void NerfDicer_OnSpawnNerf(int client, char[] nerf)
{
	if(!StrEqual(nerf, "wallhack"))
		return;
	
	SetEntPropFloat(client, Prop_Send, "m_flDetectedByEnemySensorTime", g_fTime);
	CPrintToChatAll("{darkred}[NERF] %N is visible to the enemy team for this round.", client);
}