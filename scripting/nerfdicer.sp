#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Nerf Dicer - Core"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Nerf Dicer - Core"
#define PLUGIN_URL "zipcore.net"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

#include <sourcemod>
#include <sdktools>

/* Macros */

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;%1++)\
if (IsClientInGame(%1))
	
#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;%1++)\
if (IsClientInGame(%1) && !IsFakeClient(%1))
	
#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;%1++)\
if (IsClientInGame(%1) && IsPlayerAlive(%1))

/* Forwards */

Handle g_OnSpawnNerf;

/* Handle Plugins */

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("nerf-dicer");
	
	CreateNative("NerfDicer_AddNerf", Native_AddNerf);
	
	g_OnSpawnNerf = CreateGlobalForward("NerfDicer_OnSpawnNerf", ET_Ignore, Param_String, Param_Cell);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVars();
	
	HookEvent("player_death", Event_Death);
	HookEvent("player_spawn", Event_Spawn);
}

/* ConVars */

ConVar g_cvMinKills;
int g_iMinKills;

ConVar g_cvKDRatio;
float g_fKDRatio;

/* ConVars */

void CreateConVars()
{
	CreateConVar("nerfdicer_version", PLUGIN_VERSION, "Nerf-Dicer version", FCVAR_DONTRECORD | FCVAR_PLUGIN | FCVAR_SPONLY | FCVAR_REPLICATED | FCVAR_NOTIFY);
	
	g_cvMinKills = CreateConVar("nerfdicer_kills", "5", "Min kills before nerfing.");
	g_iMinKills = GetConVarInt(g_cvMinKills);
	HookConVarChange(g_cvMinKills, Action_OnSettingsChange);
	
	g_cvKDRatio = CreateConVar("nerfdicer_kdr", "2.5", "Player above this kill/death ratio will be nerfed.");
	g_fKDRatio = GetConVarFloat(g_cvKDRatio);
	HookConVarChange(g_cvKDRatio, Action_OnSettingsChange);
	
	AutoExecConfig(true, "nerfdicer");	
}

public void Action_OnSettingsChange(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	if (cvar == g_cvMinKills)
		g_iMinKills = StringToInt(newvalue);
	else if (cvar == g_cvKDRatio)
		g_fKDRatio = StringToFloat(newvalue);
}

/* Nerf Handler */

Handle g_aNerf = null;

public int Native_AddNerf(Handle plugin, int numParams)
{
	char sNerf[32];
	GetNativeString(1, sNerf, 32);
	
	int chance = GetNativeCell(2);
	
	if(g_aNerf == null)
		g_aNerf = CreateArray(32);
	
	char sBuffer[32];
	
	for (int i = 0; i < GetArraySize(g_aNerf) - 1; i++)
	{
		GetArrayString(g_aNerf, i, sBuffer, sizeof(sBuffer));
		if (StrEqual(sBuffer, sNerf))
			RemoveFromArray(g_aNerf, i);
	}
	
	for (int i = 0; i < chance; i++)
		PushArrayString(g_aNerf, sNerf);
}

/* Events */

int g_iKills[MAXPLAYERS + 1];
int g_iDeaths[MAXPLAYERS + 1];

public void OnMapStart()
{
	LoopClients(i)
	{
		g_iDeaths[i] = 0;
		g_iKills[i] = 0;
	}
}

public void OnClientAuthorized(int client, const char[] auth)
{
	g_iDeaths[client] = 0;
	g_iKills[client] = 0;
}

public Action Event_Death(Handle event, const char[] name, bool dontBroadcast)
{
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	
	if(victim && attacker)
	{
		g_iDeaths[victim]++;
		g_iKills[attacker]++;
	}
	
	return Plugin_Continue;
}

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	CreateTimer(0.1, Timer_Nerf, GetEventInt(event, "userid"), TIMER_FLAG_NO_MAPCHANGE);
	
	return Plugin_Continue;
}

public Action Timer_Nerf(Handle timer, any data)
{
	int client = GetClientOfUserId(data);
	
	if(!client)
		return Plugin_Continue;
	
	if(g_aNerf == null || GetArraySize(g_aNerf) < 1)
		return Plugin_Continue;
	
	if(g_iMinKills > g_iKills[client] || float(g_iKills[client])/float(g_iDeaths[client]) < g_fKDRatio)
		return Plugin_Continue;
	
	char sNerf[32];
	GetArrayString(g_aNerf, GetRandomInt(0, GetArraySize(g_aNerf) - 1), sNerf, sizeof(sNerf));
	
	Call_StartForward(g_OnSpawnNerf);
	Call_PushCell(client);
	Call_PushString(sNerf);
	Call_Finish();
	
	return Plugin_Handled;
}